// Kvark med flingor
// Gott och nyttigt mellanmål med högt proteininnehåll

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // ingredient list
        enum INGREDIENTS {POWDER, JAM, QUARK, BRAN, RAISIN};

        unsigned char nbr_ingredients = RAISIN + 1;

        // weights
        double base_weight = 100; // for the breads
        double weight      = base_weight * nbr_eaters;

        // proportions
        double prop[nbr_ingredients];

        prop[JAM]    = 0.1;
        prop[POWDER] = 0.3;
        prop[QUARK]  = 5;
        prop[BRAN]   = 0.3;
        prop[RAISIN] = 0.1;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        quark(          &ingredients[QUARK],  prop[QUARK]);
        protein_powder( &ingredients[POWDER], prop[POWDER]);
        jam(            &ingredients[JAM],    prop[JAM]);
        bran_flakes(    &ingredients[BRAN],   prop[BRAN]);
        raisin(         &ingredients[RAISIN], prop[RAISIN]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "Mix and eat\n");

        return 0;
}
