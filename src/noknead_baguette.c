// Baguette, knådlös
// Helt fantastisk bra. något vattnig i smaken men osäker på vad som ska ändras.

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // weights
        double base_weight = 255; // one baguette
        double weight      = base_weight * nbr_eaters;

        // ingredient list
        enum INGREDIENTS {FLOUR, WATER, SALT, YEAST};
        unsigned char nbr_ingredients = YEAST + 1;

        // proportions
        double prop[nbr_ingredients];

        prop[FLOUR] = 1.0;
        prop[WATER] = 0.637;
        prop[YEAST] = 0.041;
        prop[SALT]  = 0.020;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        flour( &ingredients[FLOUR], prop[FLOUR]);
        water( &ingredients[WATER], prop[WATER]);
        salt(  &ingredients[SALT],  prop[SALT]);
        yeast( &ingredients[YEAST], prop[YEAST]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "0. Dissolve yeast\n"
               "1. Mix all\n"
               "2. Stir until it lets go of bunke (smidig)\n"
               "3. Rise for at least 12 h room temp lmao\n"
               "4. Cut and form some baguettes on floured surface\n"
               "5. Rise for 1.5 h room temp lmao\n"
               "6. Cut in baguettes diagonally (preferably with sharp thing)\n"
               "7. Spray or brush with water at 0, 5 and 10 min while baking\n"
               "8. Bake 275 - 300 C 15 min total with water bowl in oven\n");

        return 0;
}
