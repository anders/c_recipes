// Pitabröd
// Syrisk typ som sväller upp som en kudde och kan fyllas med saker.

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // weights
        double base_weight = 46; // large pizza
        double weight      = base_weight * nbr_eaters;

        // ingredient list
        enum INGREDIENTS {FLOUR, WATER, OIL, SALT, YEAST};
        const unsigned char nbr_ingredients = YEAST + 1;


        // proportions
        const double biga_prop = 4.0/7.0;
        double prop[nbr_ingredients];

        prop[FLOUR] = 1.000;
        prop[WATER] = 0.640;
        prop[OIL]   = 0.020;
        prop[YEAST] = 0.068;
        prop[SALT]  = 0.025;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        flour( &ingredients[FLOUR], prop[FLOUR]);
        oil(   &ingredients[OIL],   prop[OIL]);
        water( &ingredients[WATER], prop[WATER]);
        salt(  &ingredients[SALT],  prop[SALT]);
        yeast( &ingredients[YEAST], prop[YEAST]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n");
        printf("0. Dissolve yeast in water add %.1f grams flour\n", biga_prop * prop[FLOUR] * weight);
        printf("1. Mix until no klumpar, rest for 20 min\n");
        printf("2. Add oil and salt and then rest of flour (%.1f grams) in batches, knead well and reast for 2 hours\n", (1.0-biga_prop) * prop[FLOUR] * weight);
        printf("3. Divide into pieces, flatten, rest 30 min and cook in pan a few minutes per side or 230 C in oven 6 min\n");

        return 0;
}
