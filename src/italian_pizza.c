// Pizza, italiensk stil
// Italiensk stil med höga kanter och sånt. Notera ingen olja i deg. Tillsätt ovanpå italiensk stil

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // weights
        double base_weight = 170; // large pizza
        double weight      = base_weight * nbr_eaters;

        // ingredient list
        enum INGREDIENTS {FLOUR, WATER, SALT, YEAST};
        unsigned char nbr_ingredients = YEAST + 1;

        // proportions
        double prop[nbr_ingredients];

        prop[FLOUR] = 1.0;
        prop[WATER] = 0.65;
        prop[YEAST] = 0.005;
        prop[SALT]  = 0.02;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        flour( &ingredients[FLOUR], prop[FLOUR]);
        water( &ingredients[WATER], prop[WATER]);
        salt(  &ingredients[SALT],  prop[SALT]);
        yeast( &ingredients[YEAST], prop[YEAST]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "0. Dissolve yeast into water, add all, knead for short time like 2 min no flour lumps!\n"
               "1. Fold after 30, 45 and 60 min like 5 times each and form balls\n"
               "2. Rise for at least 18 h fridge lmao\n"
               "3. Form a pizza\n"
               "4. Bake as hot as possible on stone lmao\n");

        return 0;
}
