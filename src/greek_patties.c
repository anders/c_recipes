// Grekiska färsbiffar med fetaost, tzatziki och ris
// Goda färsbiffar som funkar bra som matlåda

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // ingredient list
        enum INGREDIENTS {GROUND, QUARK, FETA, RICE, CRUSH};

        unsigned char nbr_ingredients = CRUSH + 1;

        // weights
        double base_weight = 100;
        double weight      = base_weight * nbr_eaters;

        // proportions
        double prop[nbr_ingredients];

        prop[GROUND]  = 0.83;
        prop[QUARK]   = 3.3;
        prop[FETA]    = 0.5;
        prop[RICE]    = 0.5;
        prop[CRUSH]   = 1.7;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        quark(            &ingredients[QUARK],  prop[QUARK]);
        ground_beef(      &ingredients[GROUND], prop[GROUND]);
        feta(             &ingredients[FETA],   prop[FETA]);
        rice(             &ingredients[RICE],   prop[RICE]);
        crushed_tomatoes( &ingredients[CRUSH],  prop[CRUSH]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "Make patties and fry\n"
               "Boil rice\n"
               "Make tzatziki\n"
               "Boil patties in tomato sauce\n"
               "Crumble feta cheese on top\n");
        return 0;
}
