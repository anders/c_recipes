// Kvass
// Blandade resultat. Citronen gör susen men fortfarande problem med jästfällning. Bra bubbel nu.

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // weights
        double base_weight = 1000; // one kg of water
        double weight      = base_weight * nbr_eaters;

        // ingredient list
        enum INGREDIENTS {WATER, SUGAR, RAISINS_1, RAISINS_2, LEMON, BREAD, YEAST};
        unsigned char nbr_ingredients = YEAST + 1;

        double prop[nbr_ingredients];

        prop[WATER]     = 1;
        prop[BREAD]     = 0.002; // st
        prop[LEMON]     = 0.001; // squeeze
        prop[RAISINS_1] = 0.005; // st
        prop[RAISINS_2] = 0.005; // st
        prop[SUGAR]     = 0.067;
        prop[YEAST]     = 0.005;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        water(     &ingredients[WATER],     prop[WATER]);
        rye_bread( &ingredients[BREAD],     prop[BREAD]);
        lemon(     &ingredients[LEMON],     prop[LEMON]);
        raisin(    &ingredients[RAISINS_1], prop[RAISINS_1]);
        raisin(    &ingredients[RAISINS_2], prop[RAISINS_2]);
        sugar(     &ingredients[SUGAR],     prop[SUGAR]);
        yeast(     &ingredients[YEAST],     prop[YEAST]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n");
        printf("0. Toast bread hard\n"
               "1. Put in boiling water with raisins 1\n"
               "2. Leave off heat for like 10 h\n"
               "3. Remove bread and raisins 1\n"
               "4. Add sugar, yeast, lemon and raisins 2\n"
               "5. Put in suitable container\n"
               "6. To start fermentation, perhaps put in hot place for like 30 min\n"
               "7. Leave for like a week or so (remember to keep an eye on teh pressure lmao)\n"
               "8. Remove raisins 2 and put in bottle\n"
               "9. To avoid yeast precipitation from the bottom, perhaps siphon the kvass is good idea\n");

        return 0;
}
