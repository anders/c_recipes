/*ingredient structs

 name, character
 prop
 energy
 protein

*/

typedef struct {

        char * name;
        double prop;
        double energy; // kcal / 100 g
        double protein;
} ingredient;

extern void bran_flakes             (ingredient * ing, double prop);
extern void chicken                 (ingredient * ing, double prop);
extern void crushed_tomatoes       (ingredient * ing, double prop);
extern void feta                     (ingredient * ing, double prop);
extern void fish_balls              (ingredient * ing, double prop);
extern void flour                   (ingredient * ing, double prop);
extern void ground_beef            (ingredient * ing, double prop);
extern void jam                       (ingredient * ing, double prop);
extern void lemon                   (ingredient * ing, double prop);
extern void lentils                (ingredient * ing, double prop);
extern void oats                    (ingredient * ing, double prop);
extern void oil                     (ingredient * ing, double prop);
extern void protein_powder         (ingredient * ing, double prop);
extern void quark                   (ingredient * ing, double prop);
extern void raisin                 (ingredient * ing, double prop);
extern void rice                   (ingredient * ing, double prop);
extern void rye_bread              (ingredient * ing, double prop);
extern void salt                   (ingredient * ing, double prop);
extern void sugar                  (ingredient * ing, double prop);
extern void water                  (ingredient * ing, double prop);
extern void yeast                  (ingredient * ing, double prop);

extern void calc_nutrition (ingredient * ingredients, double * nutrition, unsigned char nbr_ingredients, double base_weight);
extern void print_nutrition (double * nutrition, double nbr_eaters);
extern void print_ingredients (ingredient * ingredients, unsigned char nbr_ingredients, double weight);
