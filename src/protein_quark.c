// Proteinkvark
// Mellanmål med extremt hög mängd protein per energi

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // ingredient list
        enum INGREDIENTS {QUARK, PROTEIN_POWDER};

        unsigned char nbr_ingredients = PROTEIN_POWDER + 1;

        // weights
        double base_weight = 250; // for the breads
        double weight      = base_weight * nbr_eaters;

        // proportions
        double prop[nbr_ingredients];

        prop[QUARK]          = 1.0;
        prop[PROTEIN_POWDER] = 0.125;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        quark(          &ingredients[QUARK],          prop[QUARK]);
        protein_powder( &ingredients[PROTEIN_POWDER], prop[PROTEIN_POWDER]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "Mix and eat:\n");
        return 0;
}
