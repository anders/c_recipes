// Ciabatta
// Otroligt bra med stora luftbubblor och seg mantel men jobbig, tidskrävande och kladdig att laga, särskilt utan maskin.

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // ingredient list
        enum INGREDIENTS   {FLOUR, WATER, SALT, YEAST};
        enum B_INGREDIENTS {B_FLOUR, B_WATER, B_YEAST};

        unsigned char nbr_ingredients   = YEAST   + 1;
        unsigned char B_nbr_ingredients = B_YEAST + 1;

        // weights
        double B_base_weight = 8.88; // for teh biga
        double B_weight      = B_base_weight * nbr_eaters;

        double base_weight = 36; // for the breads
        double weight      = base_weight * nbr_eaters;

        // proportions
        double prop  [nbr_ingredients];
        double B_prop[B_nbr_ingredients];

        B_prop[B_FLOUR] = 1;
        B_prop[B_WATER] = 0.80;
        B_prop[B_YEAST] = 0.07;

        prop[FLOUR]     = 1.0;
        prop[WATER]     = 0.837;
        prop[YEAST]     = 0.035;
        prop[SALT]      = 0.021;

        // crete ingredient list
        ingredient B_ingredients[nbr_ingredients];
        flour( &B_ingredients[B_FLOUR], B_prop[B_FLOUR]);
        water( &B_ingredients[B_WATER], B_prop[B_WATER]);
        yeast( &B_ingredients[B_YEAST], B_prop[B_YEAST]);

        ingredient ingredients[nbr_ingredients];
        flour( &ingredients[FLOUR], prop[FLOUR]);
        water( &ingredients[WATER], prop[WATER]);
        salt(  &ingredients[SALT],  prop[SALT]);
        yeast( &ingredients[YEAST], prop[YEAST]);

        /* calc nutrition */
        double nutrition[2];
        double temp_nutrition[2];
        calc_nutrition(B_ingredients, nutrition, B_nbr_ingredients, B_base_weight);
        calc_nutrition(ingredients, temp_nutrition, nbr_ingredients, base_weight);
        nutrition[0] += temp_nutrition[0];
        nutrition[1] += temp_nutrition[1];

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        printf("Biga: ");
        print_ingredients(B_ingredients,B_nbr_ingredients, B_weight);
        printf("Main: ");
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "0. Start with biga\n"
               "1. Dissolve yeast in water, add flour, stirr for 1-2 min\n"
               "2. Let rise overnight, at least 8h lmao\n"
               "\n");

        printf("Continue with bread\n"
               "3. Dissolve yeast in water, add biga and stir some\n"
               "4 Add salt and flour and let rest for 20 min lmao\n"
               "5 Knead a lot (at least 18 min in machine), will not be as smooth as usual doughs\n"
               "6 Rise for 3 hours, should increase a lot in size #gainz\n"
               "7 Bake 250 C for like 20-30 min, on stone if you have one lmao\n");

        return 0;
}
