#include "ingredients.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void bran_flakes (ingredient * ing, double prop)
{
        const char * name_temp = "bran flakes";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 358;
        double protein_per_100g = 11.1;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void chicken (ingredient * ing, double prop)
{
        const char * name_temp = "chicken";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 239;
        double protein_per_100g = 27;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void crushed_tomatoes (ingredient * ing, double prop)
{
        const char * name_temp = "crushed tomatoes";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 26;
        double protein_per_100g = 1.2;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void feta (ingredient * ing, double prop)
{
        const char * name_temp = "feta cheese";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 264;
        double protein_per_100g = 14;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void fish_balls (ingredient * ing, double prop)
{
        const char * name_temp = "fish balls";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 314;
        double protein_per_100g = 4.1;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void flour (ingredient * ing, double prop)
{
        const char * name_temp = "flour";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 364;
        double protein_per_100g = 10;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void ground_beef (ingredient * ing, double prop)
{
        const char * name_temp = "ground beef";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 250;
        double protein_per_100g = 17;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void jam (ingredient * ing, double prop)
{
        const char * name_temp = "jam";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 130;
        double protein_per_100g = 0;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void lemon (ingredient * ing, double prop)
{
        const char * name_temp = "lemon";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 28;
        double protein_per_100g = 1.1;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void lentils (ingredient * ing, double prop)
{
        const char * name_temp = "lentils";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 300;
        double protein_per_100g = 21;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void oats (ingredient * ing, double prop)
{
        const char * name_temp = "oats";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 369;
        double protein_per_100g = 13;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void oil (ingredient * ing, double prop)
{
        const char * name_temp = "oil";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 884;
        double protein_per_100g = 0;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void protein_powder (ingredient * ing, double prop)
{
        const char * name_temp = "protein powder";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 410;
        double protein_per_100g = 76;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void quark (ingredient * ing, double prop)
{
        const char * name_temp = "quark";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 70;
        double protein_per_100g = 11;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}


void raisin (ingredient * ing, double prop)
{
        const char * name_temp = "raisin";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 299;
        double protein_per_100g = 3.1;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void rice (ingredient * ing, double prop)
{
        const char * name_temp = "rice";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 353;
        double protein_per_100g = 8.2;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void rye_bread (ingredient * ing, double prop)
{
        const char * name_temp = "rye bread";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 28;
        double protein_per_100g = 1.1;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void salt (ingredient * ing, double prop)
{
        const char * name_temp = "salt";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 0;
        double protein_per_100g = 0;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void sugar (ingredient * ing, double prop)
{
        const char * name_temp = "sugar";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 299;
        double protein_per_100g = 3.1;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void water (ingredient * ing, double prop)
{
        const char * name_temp = "water";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 0;
        double protein_per_100g = 0;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void yeast (ingredient * ing, double prop)
{
        const char * name_temp = "yeast";
        ing->name = malloc(sizeof(char) * (strlen(name_temp) + 1));
        strcpy(ing->name, name_temp);
        ing->prop = prop;
        double  energy_per_100g = 0;
        double protein_per_100g = 0;
        ing->energy  = energy_per_100g / (double) 100; // kcal per g
        ing->protein = protein_per_100g / (double) 100; // protein per gram
        return;
}

void calc_nutrition  (ingredient * ingredients, double * nutrition, unsigned char nbr_ingredients, double base_weight)
{ /*returns nutrition per unit*/
        double energy = 0;
        double protein = 0;
        for (unsigned char i = 0; i < nbr_ingredients; i++ ) {
                protein += ingredients[i].protein * base_weight * ingredients[i].prop;
                energy += ingredients[i].energy * base_weight * ingredients[i].prop;
        }
        nutrition[0] = energy;
        nutrition[1] = protein;
        return;
}

void print_nutrition (double * nutrition, double nbr_eaters)
{
        printf("Nutrition:\n");
        printf("%5.1f kcal (%5.1f per unit)\n", nutrition[0] * nbr_eaters, nutrition[0]);
        printf("%5.1f g protein (%5.1f per unit)\n", nutrition[1] * nbr_eaters, nutrition[1]);
        printf("\n");

        return;
}

void print_ingredients (ingredient * ingredients, unsigned char nbr_ingredients, double weight)
{
        printf("Ingredients:\n");
        for (unsigned char i = 0; i < nbr_ingredients; i++ ) {
                printf("%5.1f g of %s\n", ingredients[i].prop * weight, ingredients[i].name);
        }
        printf("\n");

        return;
}
