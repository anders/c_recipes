// Ciabatta, knådlös
// Så bra å enkel jämfört med knådvarianten. Aningen dålig undersida, svampig typ. borde kanske testa att bara mjöla rejält under? Testa att baka på sten?

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // weights
        double base_weight = 136; // one bread
        double weight      = base_weight * nbr_eaters;

        // ingredient list
        enum INGREDIENTS {FLOUR, WATER, SALT, YEAST};
        unsigned char nbr_ingredients = YEAST + 1;

        // proportions
        double prop[nbr_ingredients];

        prop[FLOUR] = 1.0;
        prop[WATER] = 0.870;
        prop[YEAST] = 0.037;
        prop[SALT]  = 0.016;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        flour( &ingredients[FLOUR], prop[FLOUR]);
        water( &ingredients[WATER], prop[WATER]);
        salt(  &ingredients[SALT],  prop[SALT]);
        yeast( &ingredients[YEAST], prop[YEAST]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);
        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "1. Mix all\n"
               "2. Rise for at least 18 h room temp lmao\n"
               "3. Carefully form a some loaves\n"
               "4. Rise for 2 h room temp lmao\n"
               "5. Bake 220 C 35-45 min\n");

        return 0;
}
