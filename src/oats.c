// Havregrynsgröt
// Squatz and Oatz, bra frukost om man ska göra något tungt på förmiddagen. Funkar även som lunch om man är i knipa

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // ingredient list
        enum INGREDIENTS {OATS, JAM, RAISIN, SALT};

        unsigned char nbr_ingredients = SALT + 1;

        // weights
        double base_weight = 100; // for the breads
        double weight      = base_weight * nbr_eaters;

        // proportions
        double prop[nbr_ingredients];

        prop[OATS]   = 0.5;
        prop[JAM]    = 0.1;
        prop[RAISIN] = 0.1;
        prop[SALT]   = 0.01;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        jam(    &ingredients[JAM],    prop[JAM]);
        raisin( &ingredients[RAISIN], prop[RAISIN]);
        salt(   &ingredients[SALT],   prop[SALT]);
        oats(   &ingredients[OATS],   prop[OATS]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "0. Soak oats, raisins and salt overnight, then boil:\n"
               "1. Boil along with apples and stuff\n"
               "2. Add jam and berries and stuff lmao\n");

        return 0;
}
