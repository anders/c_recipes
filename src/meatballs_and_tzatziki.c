// Köttbullar och yoghurtsås
// Någorlunda mager men mättande måltid. Kanske behöver något mer?

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // ingredient list
        enum INGREDIENTS {LEMON, QUARK, GROUND_BEEF};

        unsigned char nbr_ingredients = 3;

        // weights
        double base_weight = 125  ; // for the breads
        double weight      = base_weight * nbr_eaters;

        // proportions
        double prop[nbr_ingredients];

        prop[GROUND_BEEF] = 1.0;
        prop[QUARK]       = 2;
        prop[LEMON]       = 0.3;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        ground_beef( &ingredients[GROUND_BEEF], prop[GROUND_BEEF]);
        lemon(       &ingredients[LEMON],       prop[LEMON]);
        quark(       &ingredients[QUARK],       prop[QUARK]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "Fry meatballs and make tzatziki\n");

        return 0;
}
