// Fiskbullar med ris
// Gudarnas föda, fiskbullar med ris.


#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // ingredient list
        enum INGREDIENTS {FISH_BALLS, RICE};

        unsigned char nbr_ingredients = 2;

        // weights
        double base_weight = 50; // for the breads
        double weight      = base_weight * nbr_eaters;

        // proportions
        double prop[nbr_ingredients];

        prop[RICE]       = 1.0;
        prop[FISH_BALLS] = 3.75;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        rice(       &ingredients[RICE],       prop[RICE]);
        fish_balls( &ingredients[FISH_BALLS], prop[FISH_BALLS]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "0. Boil rice and heat fish balls eat\n");

        return 0;
}
