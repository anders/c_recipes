// Linssallad med tzatziki
// Sallad med linser och yoghurtsås.

#include <stdio.h>
#include "ingredients.h"

int main( int argc, char *argv[] )  {

        // take number of eaters as input
        double nbr_eaters;
        if (argc != 2) {
                printf("wrong input read the readme");
                return -1;
        }
        sscanf(argv[1], "%lf", &nbr_eaters);

        // ingredient list
        enum INGREDIENTS {LENTILS, QUARK, SALT};

        unsigned char nbr_ingredients = SALT + 1;

        // weights
        double base_weight = 35; // for the breads
        double weight      = base_weight * nbr_eaters;

        // proportions
        double prop[nbr_ingredients];

        prop[LENTILS] = 1.0; // 35 g
        prop[QUARK]   = 10.0; // 350 g
        prop[SALT]    = 0.01;

        // crete ingredient list
        ingredient ingredients[nbr_ingredients];
        quark(   &ingredients[QUARK],   prop[QUARK]);
        salt(    &ingredients[SALT],    prop[SALT]);
        lentils( &ingredients[LENTILS], prop[LENTILS]);

        /* calc nutrition */
        double nutrition[2];
        calc_nutrition(ingredients, nutrition, nbr_ingredients, base_weight);

        /* print */
        print_nutrition(nutrition, nbr_eaters);
        print_ingredients(ingredients, nbr_ingredients, weight);

        printf("Instructions:\n"
               "0. Boil lentils and mix a salld into it:\n");

        return 0;
}
